package com.example.waiat.activity;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alexzh.circleimageview.CircleImageView;
import com.example.waiat.other.Message;
import com.example.waiat.adapter.MessageAdapter;
import com.example.waiat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChatActivity extends AppCompatActivity {
    private String messageReceivedID, messageReceivedName, messageReceivedImage, messageSenderID;
    private CircleImageView userImage;
    private TextView userName, userChatMessage, userLastSeen;
    private DatabaseReference RootRef;
    private ImageView sendPersonalMessageBtn;
    private EditText messageTextInput;
    private FirebaseAuth mAuth;
    private Toolbar mToolbar;
    private final List<Message> messageList = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private MessageAdapter messageAdapter;
    private RecyclerView userMessageList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        mAuth = FirebaseAuth.getInstance();
        RootRef = FirebaseDatabase.getInstance().getReference();
        messageSenderID = mAuth.getCurrentUser().getUid();
        messageReceivedID = getIntent().getExtras().get("visit_user_id").toString();
        messageReceivedName = getIntent().getExtras().get("visit_user_name").toString();
        messageReceivedImage = getIntent().getExtras().get("visit_user_avatar").toString();
        InitializeControler();
        userName.setText(messageReceivedName);
        Picasso.get().load(messageReceivedImage).into(userImage);
        sendPersonalMessageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendMessagePrivate();
            }
        });
    }

    private void SendMessagePrivate() {
        String messageText =  messageTextInput.getText().toString();
        if (TextUtils.isEmpty(messageText)){
            Toast.makeText(this, "Please enter the text ....",Toast.LENGTH_SHORT).show();
        } else {
            String messageSenderRef = "Message/"+messageSenderID+"/"+messageReceivedID;
            String messageReceivedRef = "Message/"+messageReceivedID+"/"+messageSenderID;
            DatabaseReference userMessageKeyRef = RootRef.child("Messages")
                    .child(messageSenderID)
                    .child(messageReceivedID)
                    .push();
            String messagePushID = userMessageKeyRef.getKey();
            Map messageTextBody = new HashMap();
            messageTextBody.put("message",messageText);
            messageTextBody.put("type", "text");
            messageTextBody.put("from",messageSenderID);

            Map messageBodyDetails = new HashMap();
            messageBodyDetails.put(messageSenderRef + "/" +messagePushID,messageTextBody);
            messageBodyDetails.put(messageReceivedRef + "/" +messagePushID,messageTextBody);
            RootRef.updateChildren(messageBodyDetails).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()){
                        Toast.makeText(ChatActivity.this," Message send succesfully ...",Toast.LENGTH_SHORT).show();
                    } else
                    {
                        Toast.makeText(ChatActivity.this," Message send ERROR ...",Toast.LENGTH_SHORT).show();
                    }
                    messageTextInput.setText("");
                }
            });

        }
    }

    private void InitializeControler() {
        mToolbar = findViewById(R.id.chat_bar_layout);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        final View actionBarLayout = getLayoutInflater().inflate(R.layout.custom_chat_bar, null);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setCustomView(actionBarLayout);
        userImage = findViewById(R.id.chatAvatarImage);
        userName = findViewById(R.id.chat_user_name);
        userLastSeen = findViewById(R.id.last_seen_message);
        sendPersonalMessageBtn = findViewById(R.id.send_private_message_btn);
        messageTextInput = findViewById(R.id.input_message_private);
        messageAdapter = new MessageAdapter(messageList);
        userMessageList = findViewById(R.id.personal_message);
        linearLayoutManager = new LinearLayoutManager(this);
        userMessageList.setLayoutManager(linearLayoutManager);
        userMessageList.setAdapter(messageAdapter);


    }

    private void DiplayLastSeen(){
        RootRef.child("Users").child(messageReceivedID)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.child("userState").hasChild("state")){
                            String state = dataSnapshot.child("userState").child("state").getValue().toString();
                            String date = dataSnapshot.child("userState").child("date").getValue().toString();
                            String time = dataSnapshot.child("userState").child("time").getValue().toString();
                            if (state.equals("online")){
                               userLastSeen.setText("Online");
                            } else {
                                userLastSeen.setText("Last seen: "+date+" "+time);
                            }
                        } else {
                            userLastSeen.setText("Offline");
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();
        DiplayLastSeen();
        RootRef.child("Message").child(messageSenderID).child(messageReceivedID)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        Message messages = dataSnapshot.getValue(Message.class);
                        messageList.add(messages);
                        messageAdapter.notifyDataSetChanged();
                        userMessageList.smoothScrollToPosition(userMessageList.getAdapter().getItemCount());
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.options_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId()==R.id.main_signout_options){
            mAuth.signOut();
//            SendUserToLoginActivity();
        }
        if (item.getItemId()==R.id.main_find_friends_options){
//            SendUserToFindFriendActivity();
        }
        if (item.getItemId()==R.id.main_create_group_options){
//            RequireCreateNewGroup();

        }
        if (item.getItemId()==R.id.main_settings_options){
//            SendUserToSettingsActivity();
        }
        if (item.getItemId()==android.R.id.home) {
            finish();
        }
        return true;
    }

}
