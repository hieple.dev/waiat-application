package com.example.waiat.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.waiat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.Objects;

public class LoginActivity extends AppCompatActivity {
    private FirebaseUser currentUser;
    private Button LoginButton, PhoneLoginButton;
    private EditText UserEmail, UserPassword;
    private TextView RegisterNewAccount, ForgotPassword;
    private FirebaseAuth mAuth;
    private ProgressDialog loadingBar;
    private DatabaseReference UsersRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();
        UsersRef = FirebaseDatabase.getInstance().getReference().child("Users");
        currentUser = mAuth.getCurrentUser();
        InitializeFields();
        RegisterNewAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendUserToRegisterActivity();
            }
        });
        LoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginWithAccount();
            }
        });
        PhoneLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginWithPhone();
            }
        });
    }


    private void LoginWithAccount() {
        final String emailUser = String.valueOf(UserEmail.getText());
        String passwordUser = UserPassword.getText().toString();
        if (TextUtils.isEmpty(emailUser)) {
            Toast.makeText(this, "Please enter your email ...", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(passwordUser)) {
            Toast.makeText(this, "Please enter your password ...", Toast.LENGTH_SHORT).show();
        } else {
            loadingBar.setTitle("Logining Account ....");
            loadingBar.setMessage("Please wait ....");
            loadingBar.show();
            mAuth.signInWithEmailAndPassword(emailUser, passwordUser)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                String currentUserID = mAuth.getCurrentUser().getUid();
                                String deviceToken = FirebaseInstanceId.getInstance().getToken();
                                UsersRef.child(currentUserID).child("device_token")
                                        .setValue(deviceToken)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()){
                                                    SendUserToMainActivity();
                                                    Toast.makeText(LoginActivity.this, "Login successfully with email " + emailUser, Toast.LENGTH_SHORT).show();
                                                    loadingBar.dismiss();
                                                }
                                            }
                                        });


                            } else {
                                String message = Objects.requireNonNull(task.getException()).toString();
                                loadingBar.dismiss();
                                Toast.makeText(LoginActivity.this, "Error:  " + message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
            mAuth.signInWithEmailAndPassword(emailUser, passwordUser).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    String message = e.toString();
                    loadingBar.dismiss();
                    Toast.makeText(LoginActivity.this, "Error:  " + message, Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

    private void InitializeFields() {
        LoginButton = (Button) findViewById(R.id.btnSignIn);
        PhoneLoginButton = (Button) findViewById(R.id.btnSignInPhone);
        UserEmail = (EditText) findViewById(R.id.loginInputEmail);
        UserPassword = (EditText) findViewById(R.id.loginInputPassword);
        RegisterNewAccount = (TextView) findViewById(R.id.linkRegisterAccount);
        ForgotPassword = (TextView) findViewById(R.id.linkForgotPassword);
        loadingBar = new ProgressDialog(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (currentUser != null) {
            SendUserToMainActivity();
        }
    }

    private void SendUserToMainActivity() {
        Intent mainIntent = new Intent(this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        finish();
    }

    private void SendUserToRegisterActivity() {
        Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(registerIntent);
    }

    private void LoginWithPhone() {
        Intent registerIntentPhone = new Intent(LoginActivity.this, ExamplePopupActivity.class);
        startActivity(registerIntentPhone);
    }
}
