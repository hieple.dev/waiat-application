package com.example.waiat.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.waiat.R;

public class PhoneLoginActivity extends AppCompatActivity {
    private Button btnVerify;
    private TextView  txtResetCode;
    private EditText inputPhone, inputCode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_login);
        InitializeField();
        txtResetCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtResetCode.setVisibility(View.INVISIBLE);
                inputPhone.setVisibility(View.VISIBLE);
                btnVerify.setVisibility(View.VISIBLE);
                inputCode.setVisibility(View.INVISIBLE);
                String inputPhoneText = inputPhone.getText().toString();
                if (TextUtils.isEmpty(inputPhoneText)){
                    Toast.makeText(PhoneLoginActivity.this,"Please enter your phone ...", Toast.LENGTH_SHORT).show();
                }
            }
        });
//        btnVerify.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                btnVerify.setVisibility(View.INVISIBLE);
//            }
//        });
    }

    private void InitializeField() {
        btnVerify = findViewById(R.id.btnVerify);
        txtResetCode = findViewById(R.id.tvResetCode);
        TextView linkBackLogin = findViewById(R.id.tvGoBack);
        inputCode = findViewById(R.id.inputCode);
        inputPhone = findViewById(R.id.inputPhone);
        txtResetCode.setVisibility(View.VISIBLE);
        inputPhone.setVisibility(View.VISIBLE);
        btnVerify.setVisibility(View.INVISIBLE);
        inputCode.setVisibility(View.INVISIBLE);
    }
}
