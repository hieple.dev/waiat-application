package com.example.waiat.activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.alexzh.circleimageview.CircleImageView;
import com.example.waiat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.HashMap;


public class ProfileActivity extends AppCompatActivity {
    private String receivedUserID, senderUserID, Current_State;
    private FirebaseAuth mAuth;
    private TextView otherPeopleName, otherPeopleStatus;
    private CircleImageView otherPeopleAvatar;
    private Button sendMessageRequest, cancelMessageRequest;
    private Toolbar mToolbar;
    private DatabaseReference OtherUserRef, ChatRequestRef, ContactRef, NotifycationRef;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        mAuth = FirebaseAuth.getInstance();
        OtherUserRef = FirebaseDatabase.getInstance().getReference().child("Users");
        ChatRequestRef = FirebaseDatabase.getInstance().getReference().child("Chat Request");
        ContactRef = FirebaseDatabase.getInstance().getReference().child("Contacts");
        NotifycationRef = FirebaseDatabase.getInstance().getReference().child("Notifycations");
        receivedUserID = getIntent().getExtras().get("visit_user_id").toString();
        senderUserID = mAuth.getCurrentUser().getUid();
        Toast.makeText(this,receivedUserID,Toast.LENGTH_SHORT).show();
        Toast.makeText(this,Current_State,Toast.LENGTH_SHORT).show();
        Current_State = "new";
        InitializeField();
    }

    private void InitializeField() {
        mToolbar = findViewById(R.id.other_people_bar_layout);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        otherPeopleAvatar = (CircleImageView) findViewById(R.id.otherPeopleAvatar);
        otherPeopleName = (TextView) findViewById(R.id.otherUserName);
        otherPeopleStatus = (TextView) findViewById(R.id.otherUserStatus);
        sendMessageRequest = (Button) findViewById(R.id.sendMessageButton);
        cancelMessageRequest = (Button) findViewById(R.id.decline_request_chat);
        ReceivedDataOtherPeople();
    }

    private void ReceivedDataOtherPeople() {
        OtherUserRef.child(receivedUserID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if((dataSnapshot.exists()) && (dataSnapshot.hasChild("image"))){
                    String otherUserImage = dataSnapshot.child("image").getValue().toString();
                    String otherUserName = dataSnapshot.child("name").getValue().toString();
                    String otherUserStatus = dataSnapshot.child("status").getValue().toString();
                    Picasso.get().load(otherUserImage).placeholder(R.drawable.users).into(otherPeopleAvatar);
                    otherPeopleName.setText(otherUserName);
                    otherPeopleStatus.setText(otherUserStatus);
                    getSupportActionBar().setTitle(otherUserName);
                    ManagementChatRequest();
                }else{
                    String otherUserName = dataSnapshot.child("name").getValue().toString();
                    String otherUserStatus = dataSnapshot.child("status").getValue().toString();
                    otherPeopleName.setText(otherUserName);
                    otherPeopleStatus.setText(otherUserStatus);
                    getSupportActionBar().setTitle(otherUserName);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void ManagementChatRequest() {
        ChatRequestRef.child(senderUserID)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.hasChild(receivedUserID)){
                            String request_type = dataSnapshot.child(receivedUserID).child("request_type")
                                    .getValue().toString();
                            if (request_type.equals("sent")){
                                Current_State = "request_sent";
                                sendMessageRequest.setText("Cancel Chat Request");
                            } else if (request_type.equals("received")) {
                                Current_State = "request_received";
                                sendMessageRequest.setText("Accept Chat Request");
                                cancelMessageRequest.setVisibility(View.VISIBLE);
                                cancelMessageRequest.setEnabled(true);
                                cancelMessageRequest.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        CancelChatRequest();
                                    }
                                });

                            }
                        }else {
                            ContactRef.child(senderUserID)
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.hasChild(receivedUserID))
                                            {
                                                Current_State = "friends";
                                                sendMessageRequest.setText("Remove this contact");
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
        if (!(senderUserID.equals(receivedUserID))){
            sendMessageRequest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendMessageRequest.setEnabled(true);
                    if (Current_State.equals("new")){
                        SendChatRequest();
                    }
                    if (Current_State.equals("request_sent")){
                        CancelChatRequest();
                    }
                    if (Current_State.equals("request_received")){
                        AcceptChatRequest();
                    }
                    if (Current_State.equals("friends")){
                       RemoveContact();
                    }
                }
            });
        }else {
            sendMessageRequest.setVisibility(View.INVISIBLE);
        }
    }

    private void AcceptChatRequest() {
        ContactRef.child(senderUserID).child(receivedUserID)
                .child("Contacts")
                .setValue("Saved")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            ContactRef.child(receivedUserID).child(senderUserID).child("Contacts")
                                    .setValue("Saved")
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()){
                                                ChatRequestRef.child(senderUserID).child(receivedUserID)
                                                        .removeValue()
                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if (task.isSuccessful()){
                                                                    ChatRequestRef.child(receivedUserID).child(senderUserID)
                                                                            .removeValue()
                                                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                @Override
                                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                                    sendMessageRequest.setEnabled(true);
                                                                                    Current_State = "friends";
                                                                                    sendMessageRequest.setText("Remove this contact");
                                                                                    cancelMessageRequest.setVisibility(View.INVISIBLE);
                                                                                    cancelMessageRequest.setEnabled(false);
                                                                                }
                                                                            });
                                                                }
                                                            }
                                                        });
                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    private void CancelChatRequest() {
        ChatRequestRef.child(senderUserID).child(receivedUserID)
                .removeValue()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            ChatRequestRef.child(receivedUserID).child(senderUserID)
                                    .removeValue()
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()){
                                                sendMessageRequest.setEnabled(true);
                                                Current_State = "new";
                                                sendMessageRequest.setText("Send Message");
                                                cancelMessageRequest.setVisibility(View.INVISIBLE);
                                                cancelMessageRequest.setEnabled(false);
                                            }
                                        }
                                    });
                        }
                    }
                });
    }
    private void RemoveContact() {
        ContactRef.child(senderUserID).child(receivedUserID)
                .removeValue()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            ContactRef.child(receivedUserID).child(senderUserID)
                                    .removeValue()
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()){
                                                sendMessageRequest.setEnabled(true);
                                                Current_State = "new";
                                                sendMessageRequest.setText("Send Message");
                                                cancelMessageRequest.setVisibility(View.INVISIBLE);
                                                cancelMessageRequest.setEnabled(false);
                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    private void SendChatRequest() {
        ChatRequestRef.child(senderUserID).child(receivedUserID)
            .child("request_type").setValue("sent")
            .addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()){
                        ChatRequestRef.child(receivedUserID).child(senderUserID)
                                .child("request_type").setValue("received")
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()){
                                            HashMap<String,String> chatNotifycationMap = new HashMap<>();
                                            chatNotifycationMap.put("from",senderUserID);
                                            chatNotifycationMap.put("type","request");
                                            NotifycationRef.child(receivedUserID).push()
                                                    .setValue(chatNotifycationMap)
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (task.isSuccessful()){
                                                                sendMessageRequest.setEnabled(true);
                                                                Current_State = "request_sent";
                                                                sendMessageRequest.setText("Cancel Chat Request");
                                                            }
                                                        }
                                                    });

                                        }
                                    }
                                });
                    }
                }
            });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId()==android.R.id.home) {
            finish();
        }
        return true;
    }
}
