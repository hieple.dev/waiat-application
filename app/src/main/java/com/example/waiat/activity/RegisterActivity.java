package com.example.waiat.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.waiat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {
    private Button RegisterButton;
    private EditText UserEmail, UserPassword, UserName, UserConfirmPassword;
    private TextView LoginwithAccount;
    private FirebaseAuth mAuth;
    private ProgressDialog loadingBar;
    private FirebaseUser currentUser;
    private DatabaseReference RootRef;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        RootRef = FirebaseDatabase.getInstance().getReference();
        InitializeFields();
        LoginwithAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendUserToLoginActivity();
            }
        });
        RegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateNewAccount();
            }
        });
    }

    private void CreateNewAccount() {
        String emailUser = UserEmail.getText().toString().trim();
        String passwordUser = UserPassword.getText().toString();
        String confirmPasswordUser = UserConfirmPassword.getText().toString();
        final String nameUser = UserName.getText().toString();
        if (TextUtils.isEmpty(emailUser)){
            Toast.makeText(this,"Please enter your email ...", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(passwordUser)){
            Toast.makeText(this,"Please enter your password ...", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(nameUser)){
            Toast.makeText(this,"Please enter your name ...", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(confirmPasswordUser)){
            Toast.makeText(this,"Please enter your confirm password ...", Toast.LENGTH_SHORT).show();
        } else {
            if ((passwordUser.length()<6)||confirmPasswordUser.length()<6){
                Toast.makeText(this,"Please enter your password more than 6 character ...", Toast.LENGTH_SHORT).show();
            } else if (nameUser.length()<4){
                Toast.makeText(this,"Please enter your name incorect and more than 4 character ...", Toast.LENGTH_SHORT).show();
            } else if (emailUser.length()<4){
                Toast.makeText(this,"Please enter your email incorect and more than 4 character ...", Toast.LENGTH_SHORT).show();
            } else if (confirmPasswordUser.equals(passwordUser)==false){
                Toast.makeText(this,"Please enter your confirm password the same with your password...", Toast.LENGTH_SHORT).show();
            } else {
                String checkEmail = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                if (emailUser.matches(checkEmail)) {
                    String checkName = "[a-zA-Z._. ]+";
                    if (nameUser.matches(checkName)) {
                            loadingBar.setTitle("Create New Account ...");
                            loadingBar.setMessage("Please wait we are creating new account for you ...");
                            loadingBar.setCanceledOnTouchOutside(true);
                            loadingBar.show();
                            mAuth.createUserWithEmailAndPassword(emailUser,passwordUser)
                                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            if (task.isSuccessful()){

                                                String currentUserID = mAuth.getCurrentUser().getUid();
                                                String deviceToken = FirebaseInstanceId.getInstance().getToken();
                                                RootRef.child("Users").child(currentUserID).child("device_token")
                                                        .setValue(deviceToken);
                                                RootRef.child("Users").child(currentUserID).setValue(mAuth.getCurrentUser().getEmail());
                                                SendUserToMainActivity();
                                                Toast.makeText(RegisterActivity.this, "Account created succesfully ...",Toast.LENGTH_SHORT).show();
                                                HashMap<String,Object> profileMap = new HashMap<>();
                                                profileMap.put("uid", currentUserID);
                                                profileMap.put("name", nameUser);
                                                profileMap.put("status", "I am new member of WAIAT Application");
                                                RootRef.child("Users").child(currentUserID).updateChildren(profileMap)
                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if (task.isSuccessful()){
                                                                    loadingBar.dismiss();
                                                                }else{
                                                                    loadingBar.dismiss();
                                                                }
                                                            }
                                                        });


                                            } else {
                                                String message = task.getException().toString();
                                                Toast.makeText(RegisterActivity.this, "Error is:" +message,Toast.LENGTH_SHORT).show();
                                                loadingBar.dismiss();
                                            }
                                        }
                                    });

                    }
                    else {
                        Toast.makeText(getApplicationContext(),"Invalid name address", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(),"Invalid email address", Toast.LENGTH_SHORT).show();
                }

            }
        }

    }

    private void InitializeFields() {
        RegisterButton = findViewById(R.id.btnSignUp);
        UserEmail = findViewById(R.id.registerInputEmail);
        UserName = findViewById(R.id.registerInputUserName);
        UserPassword = findViewById(R.id.registerInputPassword);
        UserConfirmPassword = findViewById(R.id.registerInputConfirmPassword);
        LoginwithAccount = findViewById(R.id.linkLoginwithAccount);
        loadingBar = new ProgressDialog(this);
    }
    private void SendUserToLoginActivity() {
        Intent loginIntent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(loginIntent);
    }
    private void SendUserToMainActivity() {
        Intent mainIntent = new Intent(RegisterActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        finish();
    }
}
