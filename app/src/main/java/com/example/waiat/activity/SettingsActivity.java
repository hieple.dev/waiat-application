package com.example.waiat.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alexzh.circleimageview.CircleImageView;
import com.example.waiat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.HashMap;

public class SettingsActivity extends AppCompatActivity {
    private Button UpdateProfile;
    private EditText EditInputUser, EditInputStatus;
    private CircleImageView AvatarUser;
    private TextView showUserName;
    private String currentUserID;
    private Toolbar mToolbar;
    private FirebaseAuth mAuth;
    private DatabaseReference RootRef;
    private static final int GalleryPick = 1;
    private StorageReference UserProfileImageRef;
    private ProgressDialog loaddingBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        mAuth = FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();
        RootRef = FirebaseDatabase.getInstance().getReference();
        UserProfileImageRef = FirebaseStorage.getInstance().getReference().child("Profile Images");
        InitializeField();
        GetUserInfo();
        UpdateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateInfoProfile();
            }
        });
        AvatarUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent();
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, GalleryPick);
            }
        });
    }

    private void GetUserInfo() {
        RootRef.child("Users").child(currentUserID)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if ((dataSnapshot.exists())&&(dataSnapshot.hasChild("name")&&dataSnapshot.hasChild("image"))){
                            String getDataUser = dataSnapshot.child("name").getValue().toString();
                            String getDataStatus = dataSnapshot.child("status").getValue().toString();
                            String getAvatarImage = dataSnapshot.child("image").getValue().toString();
                            showUserName.setText(getDataUser);
                            EditInputUser.setText(getDataUser);
                            EditInputStatus.setText(getDataStatus);
                            Picasso.get().load(getAvatarImage).into(AvatarUser);
                        } else if ((dataSnapshot.exists())&&(dataSnapshot.hasChild("name"))){
                            String getDataUser = dataSnapshot.child("name").getValue().toString();
                            String getDataStatus = dataSnapshot.child("status").getValue().toString();
                            showUserName.setText(getDataUser);
                            EditInputUser.setText(getDataUser);
                            EditInputStatus.setText(getDataStatus);

                        } else {
                            Toast.makeText(SettingsActivity.this, "Please set & update your information ...",Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
    }
    private void UpdateInfoProfile() {
        String editInputUser = EditInputUser.getText().toString();
        String editInputStatus = EditInputStatus.getText().toString();
        if (TextUtils.isEmpty(editInputUser)){
            Toast.makeText(this, "Please enter your information ...", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(editInputStatus)){
                Toast.makeText(this, "Please enter your information ...", Toast.LENGTH_SHORT).show();
            } else {
            HashMap<String,Object> profileMap = new HashMap<>();
            profileMap.put("uid", currentUserID);
            profileMap.put("name", editInputUser);
            profileMap.put("status", editInputStatus);
            RootRef.child("Users").child(currentUserID).updateChildren(profileMap)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){
//                                SendUserToMainActivity();
                                Toast.makeText(SettingsActivity.this,"Update info successfully ...", Toast.LENGTH_SHORT).show();
                            }else{
                                String message = task.getException().toString();
                                Toast.makeText(SettingsActivity.this,"Error: "+message, Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
        }


    }
    private void InitializeField() {
        mToolbar = findViewById(R.id.setting_bar_layout);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        if (getSupportActionBar() != null) getSupportActionBar().setTitle("Setting");
        UpdateProfile = findViewById(R.id.updateProfile);
        EditInputUser = findViewById(R.id.editInputUser);
        EditInputStatus = findViewById(R.id.editInputStatus);
        AvatarUser = findViewById(R.id.avatarUser);
        showUserName = findViewById(R.id.showUserName);
        loaddingBar = new ProgressDialog(this);
    }
    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId()==android.R.id.home) {
            finish();
        }
        return true;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                loaddingBar.setTitle("Set Profile Image");
                loaddingBar.setMessage("Pleas wait, your profile image is updating ...");
                loaddingBar.setCanceledOnTouchOutside(false);
                loaddingBar.show();
                if (requestCode == GalleryPick) {
                    Uri selectedImageUri = data.getData();
                    final String path = getPathFromURI(selectedImageUri);
                    if (path != null) {
                        File f = new File(path);
                        selectedImageUri = Uri.fromFile(f);
                    }
                    final StorageReference filePath = UserProfileImageRef.child(currentUserID+".jpg");
                    filePath.putFile(selectedImageUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                            if(task.isSuccessful()){
                                filePath.getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Uri> task) {
                                        final String downloadUrl = task.getResult().toString();
                                        RootRef.child("Users").child(currentUserID).child("image")
                                                .setValue(downloadUrl)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if(task.isSuccessful()){

                                                            Toast.makeText(SettingsActivity.this,"Up ok",Toast.LENGTH_SHORT).show();
                                                            loaddingBar.dismiss();
                                                        }else {
                                                            String message = task.getException().toString();
                                                            Toast.makeText(SettingsActivity.this,""+message,Toast.LENGTH_SHORT).show();
                                                            loaddingBar.dismiss();
                                                        }
                                                    }
                                                });
                                    }
                                });
                                loaddingBar.dismiss();
                            } else {
                                String message = task.getException().toString();
                                Toast.makeText(SettingsActivity.this,""+message,Toast.LENGTH_SHORT).show();
                                loaddingBar.dismiss();
                            }
                        }
                    });

                }
            }
        } catch (Exception e) {
            Log.e("FileSelectorActivity", "File select error", e);
        }
    }
}
