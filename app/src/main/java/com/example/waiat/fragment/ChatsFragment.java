package com.example.waiat.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alexzh.circleimageview.CircleImageView;
import com.example.waiat.other.Contacts;
import com.example.waiat.R;
import com.example.waiat.activity.ChatActivity;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChatsFragment extends Fragment {

    private View PersonalChatsView;
    private RecyclerView chatsList;
    private DatabaseReference ChatsRef, UsersRef;
    private FirebaseAuth mAuth;
    private String currentUserID, getImage;
    public ChatsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        PersonalChatsView =  inflater.inflate(R.layout.fragment_chats, container, false);
        mAuth = FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();
        ChatsRef = FirebaseDatabase.getInstance().getReference().child("Contacts").child(currentUserID);
        UsersRef = FirebaseDatabase.getInstance().getReference().child("Users");
        chatsList = PersonalChatsView.findViewById(R.id.chat_lists);
        chatsList.setLayoutManager(new LinearLayoutManager(getContext()));
        return PersonalChatsView;
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseRecyclerOptions<Contacts> options =
                new FirebaseRecyclerOptions.Builder<Contacts>()
                .setQuery(ChatsRef,Contacts.class)
                .build();
        FirebaseRecyclerAdapter<Contacts,ChatsHolderView> adapter =
              new FirebaseRecyclerAdapter<Contacts, ChatsHolderView>(options) {
                  @Override
                  protected void onBindViewHolder(@NonNull final ChatsHolderView holder, int position, @NonNull Contacts model) {
                        final String usersIDs = getRef(position).getKey();
                        UsersRef.child(usersIDs).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                               if (dataSnapshot.exists()){
                                   if (dataSnapshot.hasChild("image")){
                                       getImage = dataSnapshot.child("image").getValue().toString();
                                       Picasso.get().load(getImage).placeholder(R.drawable.users).into(holder.profileImage);
                                   }

                                   final String getName = dataSnapshot.child("name").getValue().toString();
                                   final String getStatus = dataSnapshot.child("status").getValue().toString();
                                   holder.userName.setText(getName);
                                   if (dataSnapshot.child("userState").hasChild("state")){
                                       String state = dataSnapshot.child("userState").child("state").getValue().toString();
                                       String date = dataSnapshot.child("userState").child("date").getValue().toString();
                                       String time = dataSnapshot.child("userState").child("time").getValue().toString();
                                       if (state.equals("online")){
                                           holder.userStatus.setText("Online");
                                       } else {
                                           holder.userStatus.setText("Last seen: "+date+" "+time);
                                       }
                                   } else {
                                       holder.userStatus.setText("Offline");
                                   }


                                   holder.itemView.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View v) {
                                           Intent chatInter = new Intent(getContext(), ChatActivity.class);
                                           chatInter.putExtra("visit_user_id",usersIDs);
                                           chatInter.putExtra("visit_user_name",getName);
                                           chatInter.putExtra("visit_user_avatar",getImage);
                                           startActivity(chatInter);
                                       }
                                   });
                               }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                  }

                  @NonNull
                  @Override
                  public ChatsHolderView onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                      View view = LayoutInflater.from(getContext()).inflate(R.layout.users_display_layout,viewGroup,false);
                      ChatsHolderView holder = new ChatsHolderView(view);
                      return holder;
                  }
              };
        chatsList.setAdapter(adapter);
        adapter.startListening();
    }
    public static class ChatsHolderView extends RecyclerView.ViewHolder{
        TextView userName, userStatus;
        CircleImageView profileImage;
        public ChatsHolderView(@NonNull View itemView) {

            super(itemView);
            profileImage = itemView.findViewById(R.id.userProfileLayout);
            userName = itemView.findViewById(R.id.userNameLayout);
            userStatus = itemView.findViewById(R.id.userStatusLayout);
        }
    }
}
