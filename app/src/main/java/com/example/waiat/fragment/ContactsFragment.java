package com.example.waiat.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alexzh.circleimageview.CircleImageView;
import com.example.waiat.other.Contacts;
import com.example.waiat.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class ContactsFragment extends Fragment {

    private View ContactsView;
    private RecyclerView myContactList;
    private DatabaseReference ContactsRef, UsersRef;
    private FirebaseAuth mAuth;
    private String currentUserID;

    final static Integer countRequest = 0;
    public ContactsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mAuth = FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();
        ContactsRef = FirebaseDatabase.getInstance().getReference().child("Contacts").child(currentUserID);
        UsersRef = FirebaseDatabase.getInstance().getReference().child("Users");
        ContactsView = inflater.inflate(R.layout.fragment_contacts, container, false);
        myContactList = ContactsView.findViewById(R.id.contact_list);
        myContactList.setLayoutManager(new LinearLayoutManager(getContext()));
        return ContactsView;

    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseRecyclerOptions options =
                new FirebaseRecyclerOptions.Builder<Contacts>()
                .setQuery(ContactsRef, Contacts.class)
                .build();
        FirebaseRecyclerAdapter<Contacts,ContactsViewHolder> adapter =
                new FirebaseRecyclerAdapter<Contacts, ContactsViewHolder>(options) {
                    @Override
                    protected void onBindViewHolder(@NonNull final ContactsViewHolder holder, int position, @NonNull Contacts model) {
                        String userIDs = getRef(position).getKey();
                        UsersRef.child(userIDs).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                               if (dataSnapshot.exists()){
                                   if (dataSnapshot.child("userState").hasChild("state")){
                                       String state = dataSnapshot.child("userState").child("state").getValue().toString();
                                       String date = dataSnapshot.child("userState").child("date").getValue().toString();
                                       String time = dataSnapshot.child("userState").child("time").getValue().toString();
                                       if (state.equals("online")){
                                           holder.onlineIcon.setVisibility(View.VISIBLE);
                                       } else {
                                           holder.onlineIcon.setVisibility(View.INVISIBLE);
                                       }
                                   } else {
                                       holder.onlineIcon.setVisibility(View.INVISIBLE);
                                   }

                                   if (dataSnapshot.hasChild("image")){
                                       String profileAvatar = dataSnapshot.child("image").getValue().toString();
                                       String profileName = dataSnapshot.child("name").getValue().toString();
                                       String profileStatus = dataSnapshot.child("status").getValue().toString();
                                       holder.userName.setText(profileName);
                                       holder.userStatus.setText(profileStatus);
                                       Picasso.get().load(profileAvatar).placeholder(R.drawable.users).into(holder.profileImage);
                                   }
                                   else {
                                       String profileName = dataSnapshot.child("name").getValue().toString();
                                       String profileStatus = dataSnapshot.child("status").getValue().toString();
                                       holder.userName.setText(profileName);
                                       holder.userStatus.setText(profileStatus);
                                   }
                               }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }

                    @NonNull
                    @Override
                    public ContactsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.users_display_layout, viewGroup, false);
                        ContactsViewHolder viewHolder = new ContactsViewHolder(view);
                        return  viewHolder;
                    }
                };
        myContactList.setAdapter(adapter);
        adapter.startListening();
    }

    public static class ContactsViewHolder extends RecyclerView.ViewHolder{
        TextView userName, userStatus;
        CircleImageView profileImage;
        ImageView onlineIcon;

        public ContactsViewHolder(@NonNull View itemView) {
            super(itemView);
            userName = itemView.findViewById(R.id.userNameLayout);
            userStatus = itemView.findViewById(R.id.userStatusLayout);
            profileImage = itemView.findViewById(R.id.userProfileLayout);
            onlineIcon = itemView.findViewById(R.id.statusActive);
        }
    }
}
